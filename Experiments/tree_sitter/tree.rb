require 'tree_sitter'

# Define language objects (assuming gems for parsers are installed)
PYTHON_LANGUAGE = TreeSitter::Language.new('python')
GO_LANGUAGE = TreeSitter::Language.new('go')
JAVASCRIPT_LANGUAGE = TreeSitter::Language.new('javascript')
JAVA_LANGUAGE = TreeSitter::Language.new('java')

# Function to find child node for a specific line number
def get_child(node, line_number)
  return nil unless node.children?

  node.children.each do |child|
    return child if (child.start_position[0] + 1 <= line_number) && (line_number <= child.end_position[0] + 1)
  end

  nil
end

# Function to find enclosing function definition
def get_enclosing_definition(code, language, line_number)
  parser = TreeSitter::Parser.new(language)
  tree = parser.parse(code)
  current_node = tree.root_node

  loop do
    child_node = get_child(current_node, line_number)
    return child_node if child_node

    break unless current_node.respond_to?(:parent)
    current_node = current_node.parent
  end

  nil
end

# Function to get features (modified for readability)
def get_features(file_path, language, line_number, num_parents = 1)
  code = File.read(file_path)
  enclosing_node = get_enclosing_definition(code, language, line_number)

  if enclosing_node
    num_parents.times do |i|
      puts "Line found inside a #{enclosing_node.type} (Level #{i + 1}):"
      puts enclosing_node.text.encode('UTF-8')  # Assuming UTF-8 encoding

      enclosing_node = enclosing_node.parent if enclosing_node.parent
    end
  else
    puts "No enclosing definition found."
  end
end

# Example usage
get_features('test_files/test.py', PYTHON_LANGUAGE, 252, 1)
