# Research papers:

### To do:
Contrastive loss functions:
* Constellation loss ![alt text](image.png)
* Magnet loss ![alt text](image-1.png)
* Ranked list loss ![alt text](image-2.png)
* InfoNCE

Architectures:
* IJEPA
* AJEPA

Augmentations:
* SpliceOut
* SpecAugment

Done:
* Audio Barlow twins
* Barlow twins
* BYOL
* Combinational loss
* Contrastive learning framework
* MOCO v1, v2, v3
* N-pair loss
* Optimizing audio augmentations for CL
* SimCLR
* SimSiam
* Supervised contrastive learning (SupCon)
* SwAV
* wav2vec